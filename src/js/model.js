! function(window) {
	'use strict';

	const API_BASE_URL = "http://swapi.co/api/";

	function Model() {

	}

	Model.prototype.getDataByResource = function(resource, callback) {
		$call_api(API_BASE_URL + resource, callback);
	}

	Model.prototype.getDataByUrl = function(url, callback) {
		$call_api(url, callback);
	}

	// Export to window
	window.app = window.app || {};
	window.app.Model = Model;

}(window);