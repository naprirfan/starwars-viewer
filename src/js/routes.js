! function(window) {
	'use strict';

	/*
		DEFINE YOUR ROUTES HERE
		Key = url hash. e.g : http://localhost#home
		Value = controller action name. Don't forget to add one in the controller
		Convention : DOM Root (in index jade) 
			for every action will be : [Action name] + "page"
			.e.g : #homepage, #detailpage, etc. 
			Please choose your action name carefully
	*/
	const DEFAULT_ACTION = "home";
	var routes = {
		""        : "home",
		"detail"  : "detail"
	};

	function Routes() {
		
	}

	Routes.prototype.getAllRoutes = function() {
		return routes;
	}

	Routes.prototype.getActionByHash = function(hash) {
		hash = hash.split('#');
		return routes[hash[1]] === undefined ? DEFAULT_ACTION : routes[hash[1]];
	}

	Routes.prototype.invoke = function(hash) {
		var controller = new app.Controller;
		var action = Routes.prototype.getActionByHash(hash);
		
		//if there's more than one hash, than pass the hash along to the controller
		// e.g : #detail#http://swapi.co/species/1/
		hash = hash.split('#');
		if (hash.length > 2) {
			hash = hash.join('#');
			controller[action](hash);
		}
		else {
			controller[action]();	
		}
		
	}

	// Export to window
	window.app = window.app || {};
	window.app.Routes = Routes;

}(window);
