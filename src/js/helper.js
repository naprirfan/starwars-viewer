! function(window){
	'use strict';

	// addEventListener wrapper:
	window.$on = function (target, type, callback, useCapture) {
		target.addEventListener(type, callback, !!useCapture);
	};

	//call API method
	window.$call_api = function(url, callback) {
		var request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.onload = function() {
			var data = JSON.parse(request.responseText);
		  	callback(data);
		};
		request.send();
	}

	//animation
	window.$animate = function(elmId, animationClass, classRemovalDelay) {
		var elm = document.getElementById(elmId);

		elm.classList.add(animationClass);

		setTimeout(function() {
			elm.classList.remove(animationClass);

			//hack, otherwise the animation won't work the second time
      		elm.offsetWidth = elm.offsetWidth;
		},classRemovalDelay);
	}

}(window);