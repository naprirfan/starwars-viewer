! function(window){
	'use strict';

	const LAZYLOAD_HEIGHT_THRESHOLD = 500;
	var model = new app.Model;
	var current_page = "";
	var isCallingApi = false;

	//this is the starting point.
	//request will be routed based on its hash.
	//router will then pass the request to controller
	function invoke() {
		var routes = new app.Routes;
		current_page = routes.getActionByHash(document.location.hash);
		routes.invoke(document.location.hash);
	}

	//$on = addEventListener. Look at helper.js
	$on(window, 'load', invoke);
	$on(window, 'hashchange', invoke);

	//lazyload
	$on(window, 'scroll', function(){
		if (
			((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - LAZYLOAD_HEIGHT_THRESHOLD))
			&& current_page == "home"
			&& !isCallingApi
		) 
		{
			
	        var homepage = document.getElementById("homepage");
	        var nextPage = homepage.getAttribute("data-next");

	        if (nextPage !== "null") {
	        	isCallingApi = true;
	        	//create loading element
	        	var loader = document.createElement('div');
	        	loader.className = "loader";
	        	loader.id = "loader";
	        	homepage.appendChild(loader);

	        	model.getDataByUrl(nextPage, function(data){
	        		var view = new app.View;
	        		view.renderHomepage(data);

	        		//setting vars
	        		isCallingApi = false;
	        		homepage.removeChild(loader);
	        	});
	        }
	    }
	});

}(window);