! function(window) {
	'use strict';

	function Controller() {
		this.model = new app.Model;
	}

	function showLoading() {
		console.log('show loading');
		//hide all page
		var r = new app.Routes;
		var routes = r.getAllRoutes();
		for (var key in routes) {
			var dom = document.getElementById(routes[key] + "page");
			if (dom) {
				dom.setAttribute("style", "display: none;");
			}
		}
		
		document.getElementById("mainTitle").setAttribute("style", "display: none;");
		//show loading
		document.getElementById("loading").setAttribute("style", "display: block");
	}

	Controller.prototype.showPage = function(page) {
		//hide loading
		document.getElementById("loading").setAttribute("style", "display: none;");
		//show title
		document.getElementById("mainTitle").setAttribute("style", "display: block;");
		//show page
		document.getElementById(page).setAttribute("style", "display: block");
	}

	function emptyElementById(domId) {
		var elm = document.getElementById(domId);
		while(elm.hasChildNodes()) {
			elm.removeChild(elm.lastChild);
		}
	}

	Controller.prototype.home = function() {
		showLoading();

		if(document.getElementById("homepage").hasChildNodes()) {
			Controller.prototype.showPage("homepage");
		}
		else {
			this.model.getDataByResource("species", function(data){
				var view = new app.View;
				view.renderHomepage(data);
				Controller.prototype.showPage("homepage");
			});
		}
	}

	Controller.prototype.detail = function(args) {
		//clear previous detail page
		emptyElementById('detailpage');
		showLoading();
		args = args.split('#');
		this.model.getDataByUrl(args[2], function(data){
			var view = new app.View;
			view.renderDetailpage(data);
			Controller.prototype.showPage("detailpage");
		});
	}

	// Export to window
	window.app = window.app || {};
	window.app.Controller = Controller;

}(window);