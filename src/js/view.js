! function(window) {
	'use strict';

	function View() {
		
	}

	View.prototype.renderDetailpage = function(data) {
		//get root
		var container = document.getElementById("detailpage");
		//get template
		var t = document.querySelector('#detail');
		//mapping every data key to html tag class in template
		for(var key in data) {
			var tag = t.content.querySelector("." + key);
			if (tag) {
				tag.textContent = key.toUpperCase() + ": " + data[key];		
			}
		}

		//adding new node to container
		var clone = document.importNode(t.content, true);
		container.appendChild(clone);
	}

	View.prototype.renderHomepage = function(data) {
		//get root
		var container = document.getElementById("homepage");
		//set for next page
		console.log('data.next = ' + data.next);
		container.setAttribute("data-next", data.next);

		//get template
		var t = document.querySelector('#home');
		for (var i = 0; i < data.results.length; i++) {
			//mapping every data key to html tag class in template
			var obj = data.results[i];
			for(var key in obj) {
				var tag = t.content.querySelector("." + key);
				if (tag) {
					tag.textContent = key.toUpperCase() + ": " + data.results[i][key];		
				}
			}

			var a = t.content.querySelectorAll("a");
			a[0].setAttribute("href", "#detail#" + obj.url);

			//adding new node to container
			var clone = document.importNode(t.content, true);
			container.appendChild(clone);	
		}
	}

	// Export to window
	window.app = window.app || {};
	window.app.View = View;

}(window);