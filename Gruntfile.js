module.exports = function (grunt) {
	grunt.initConfig({
		concat: {
			dist: {
				src: ['src/js/helper.js','src/js/routes.js','src/js/model.js','src/js/view.js','src/js/controller.js','src/js/app.js',],
				dest: 'dist/js/script.min.js',
			},
		},
		uglify: {
	    	my_target: {
	      		files: {
	        		'dist/js/script.min.js': ['dist/js/script.min.js']
	      		}
	    	}
	  	},
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	
	grunt.registerTask('default', ['concat', 'uglify']);
}